-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2021 at 09:43 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cards`
--

-- --------------------------------------------------------

--
-- Table structure for table `cartas`
--

CREATE TABLE `cartas` (
  `idCartas` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cartas`
--

INSERT INTO `cartas` (`idCartas`, `tipo`, `estado`) VALUES
(13, 2, 0),
(14, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cartascontenidos`
--

CREATE TABLE `cartascontenidos` (
  `idContenido` int(11) NOT NULL,
  `idCarta` int(11) NOT NULL,
  `contenido` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cartascontenidos`
--

INSERT INTO `cartascontenidos` (`idContenido`, `idCarta`, `contenido`) VALUES
(22, 13, 'buenas'),
(23, 13, 'x'),
(24, 14, 'por ejemplo'),
(25, 14, '1'),
(26, 14, '2');

-- --------------------------------------------------------

--
-- Table structure for table `cartaseleccionada`
--

CREATE TABLE `cartaseleccionada` (
  `id` int(11) NOT NULL,
  `idcarta` int(11) NOT NULL,
  `idjugador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cartaseleccionada`
--

INSERT INTO `cartaseleccionada` (`id`, `idcarta`, `idjugador`) VALUES
(1, 2, 2),
(2, 3, 2),
(3, 4, 2),
(4, 2, 2),
(5, 3, 2),
(6, 4, 2),
(7, 2, 2),
(8, 3, 2),
(9, 4, 2),
(10, 2, 2),
(11, 2, 2),
(12, 3, 2),
(13, 4, 2),
(14, 2, 2),
(15, 2, 2),
(16, 4, 2),
(17, 3, 2),
(18, 2, 3),
(19, 4, 3),
(20, 2, 3),
(21, 2, 3),
(22, 2, 3),
(23, 2, 3),
(24, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `juez`
--

CREATE TABLE `juez` (
  `idJuez` int(11) NOT NULL,
  `idJugador` int(11) NOT NULL,
  `numeroTurno` int(11) NOT NULL,
  `idCarta` int(11) NOT NULL,
  `idJugadorGanador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `juez`
--

INSERT INTO `juez` (`idJuez`, `idJugador`, `numeroTurno`, `idCarta`, `idJugadorGanador`) VALUES
(1, 1, 1, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jugador`
--

CREATE TABLE `jugador` (
  `idJugador` int(11) NOT NULL,
  `nombreUsuario` varchar(50) NOT NULL,
  `turno` int(11) NOT NULL,
  `puntuación` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jugador`
--

INSERT INTO `jugador` (`idJugador`, `nombreUsuario`, `turno`, `puntuación`) VALUES
(1, 'jugador5', 1, 0),
(2, 'jugador2', 2, 0),
(3, 'jugador3', 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `manojugador`
--

CREATE TABLE `manojugador` (
  `idMano` int(11) NOT NULL,
  `idCarta` int(11) NOT NULL,
  `idJugador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manojugador`
--

INSERT INTO `manojugador` (`idMano`, `idCarta`, `idJugador`) VALUES
(75, 2, 3),
(76, 2, 3),
(77, 2, 1),
(78, 3, 1),
(79, 4, 1),
(80, 2, 3),
(81, 3, 3),
(82, 4, 3),
(83, 2, 3),
(84, 3, 3),
(85, 4, 3),
(86, 2, 1),
(87, 3, 1),
(88, 4, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cartas`
--
ALTER TABLE `cartas`
  ADD PRIMARY KEY (`idCartas`);

--
-- Indexes for table `cartascontenidos`
--
ALTER TABLE `cartascontenidos`
  ADD PRIMARY KEY (`idContenido`);

--
-- Indexes for table `cartaseleccionada`
--
ALTER TABLE `cartaseleccionada`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `juez`
--
ALTER TABLE `juez`
  ADD PRIMARY KEY (`idJuez`);

--
-- Indexes for table `jugador`
--
ALTER TABLE `jugador`
  ADD PRIMARY KEY (`idJugador`);

--
-- Indexes for table `manojugador`
--
ALTER TABLE `manojugador`
  ADD PRIMARY KEY (`idMano`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cartas`
--
ALTER TABLE `cartas`
  MODIFY `idCartas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `cartascontenidos`
--
ALTER TABLE `cartascontenidos`
  MODIFY `idContenido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `cartaseleccionada`
--
ALTER TABLE `cartaseleccionada`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `juez`
--
ALTER TABLE `juez`
  MODIFY `idJuez` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jugador`
--
ALTER TABLE `jugador`
  MODIFY `idJugador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `manojugador`
--
ALTER TABLE `manojugador`
  MODIFY `idMano` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
