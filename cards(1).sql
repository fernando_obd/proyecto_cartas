-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-10-2021 a las 02:39:06
-- Versión del servidor: 10.4.20-MariaDB
-- Versión de PHP: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cards`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cartas`
--

CREATE TABLE `cartas` (
  `idCartas` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cartas`
--

INSERT INTO `cartas` (`idCartas`, `tipo`, `estado`) VALUES
(1, 1, 0),
(2, 0, 1),
(3, 0, 1),
(4, 0, 1),
(5, 1, 0),
(6, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cartascontenidos`
--

CREATE TABLE `cartascontenidos` (
  `idContenido` int(11) NOT NULL,
  `idCarta` int(11) NOT NULL,
  `contenido` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cartascontenidos`
--

INSERT INTO `cartascontenidos` (`idContenido`, `idCarta`, `contenido`) VALUES
(1, 1, 'ujeres encomerciales decomerciales deyogurt.yogurt.'),
(2, 2, 'Connotaciones clasistas.'),
(3, 3, 'una frase'),
(4, 3, 'otra frase'),
(5, 4, 'alfo'),
(6, 5, 'Mujeres encomerciales decomerciales deyogurt.yogurt.'),
(7, 5, 'awevo'),
(8, 6, 'una carta nueva.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cartaseleccionada`
--

CREATE TABLE `cartaseleccionada` (
  `id` int(11) NOT NULL,
  `idcarta` int(11) NOT NULL,
  `idjugador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cartaseleccionada`
--

INSERT INTO `cartaseleccionada` (`id`, `idcarta`, `idjugador`) VALUES
(1, 2, 2),
(2, 3, 2),
(3, 4, 2),
(4, 2, 2),
(5, 3, 2),
(6, 4, 2),
(7, 2, 2),
(8, 3, 2),
(9, 4, 2),
(10, 2, 2),
(11, 2, 2),
(12, 3, 2),
(13, 4, 2),
(14, 2, 2),
(15, 2, 2),
(16, 4, 2),
(17, 3, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `juez`
--

CREATE TABLE `juez` (
  `idJuez` int(11) NOT NULL,
  `idJugador` int(11) NOT NULL,
  `numeroTurno` int(11) NOT NULL,
  `idCarta` int(11) NOT NULL,
  `idJugadorGanador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `juez`
--

INSERT INTO `juez` (`idJuez`, `idJugador`, `numeroTurno`, `idCarta`, `idJugadorGanador`) VALUES
(1, 1, 1, 5, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jugador`
--

CREATE TABLE `jugador` (
  `idJugador` int(11) NOT NULL,
  `nombreUsuario` varchar(50) NOT NULL,
  `turno` int(11) NOT NULL,
  `puntuación` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `jugador`
--

INSERT INTO `jugador` (`idJugador`, `nombreUsuario`, `turno`, `puntuación`) VALUES
(1, 'jugador5', 1, 0),
(2, 'jugador2', 2, 0),
(3, 'jugador3', 3, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `manojugador`
--

CREATE TABLE `manojugador` (
  `idMano` int(11) NOT NULL,
  `idCarta` int(11) NOT NULL,
  `idJugador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `manojugador`
--

INSERT INTO `manojugador` (`idMano`, `idCarta`, `idJugador`) VALUES
(64, 2, 1),
(65, 3, 1),
(66, 4, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cartas`
--
ALTER TABLE `cartas`
  ADD PRIMARY KEY (`idCartas`);

--
-- Indices de la tabla `cartascontenidos`
--
ALTER TABLE `cartascontenidos`
  ADD PRIMARY KEY (`idContenido`);

--
-- Indices de la tabla `cartaseleccionada`
--
ALTER TABLE `cartaseleccionada`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `juez`
--
ALTER TABLE `juez`
  ADD PRIMARY KEY (`idJuez`);

--
-- Indices de la tabla `jugador`
--
ALTER TABLE `jugador`
  ADD PRIMARY KEY (`idJugador`);

--
-- Indices de la tabla `manojugador`
--
ALTER TABLE `manojugador`
  ADD PRIMARY KEY (`idMano`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cartas`
--
ALTER TABLE `cartas`
  MODIFY `idCartas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `cartascontenidos`
--
ALTER TABLE `cartascontenidos`
  MODIFY `idContenido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `cartaseleccionada`
--
ALTER TABLE `cartaseleccionada`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `juez`
--
ALTER TABLE `juez`
  MODIFY `idJuez` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `jugador`
--
ALTER TABLE `jugador`
  MODIFY `idJugador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `manojugador`
--
ALTER TABLE `manojugador`
  MODIFY `idMano` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
