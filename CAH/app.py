﻿from flask import Flask, render_template, request, flash, redirect, url_for
from flask_mysqldb import MySQL

import random

app = Flask(__name__)
# mysql
app.secret_key = 'dnksadsaj'
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'cards'
mysql = MySQL(app)
player_array = []
contplayer=0
crespuestas=0
vjuez=False
black_card = 5
band = 0
juez = 1


def reiniciar_bd():
    cursor = mysql.connection.cursor()
    cursor.execute('UPDATE jugador SET turno=0, puntuación=0')
    cursor.execute('DELETE FROM manojugador')
    cursor.execute('DELETE FROM juez')
    cursor.execute('DELETE FROM cartaseleccionada')
    mysql.connection.commit()
    cursor.close

@app.route('/')
def index():
    # id=player_random()
    cursor = mysql.connection.cursor()
    # cursor.execute('SELECT contenido, idCarta FROM `cartascontenidos` cc, `cartas` c WHERE c.idCartas=cc.idCarta;')
    cursor.execute('SELECT GROUP_CONCAT(contenido) contenido, c.idCartas FROM cartas c, cartascontenidos cc WHERE '
                   'c.idCartas = cc.idCarta GROUP BY cc.idCarta;')
    data = cursor.fetchall()
    cursor.close()
    return render_template('inicio.html')
    # return render_template('inicio.html', cartas=data)
    # return redirect('jugar/{}'.format(id))


@app.route('/indexado')
def prueba():
    cursor = mysql.connection.cursor()
    cursor.execute(
        'SELECT GROUP_CONCAT(contenido) contenido, c.idCartas FROM cartas c, cartascontenidos cc WHERE c.idCartas = '
        'cc.idCarta GROUP BY cc.idCarta;')
    data = cursor.fetchall()
    cursor.close()
    return render_template('index.html', cartas=data)


@app.route('/play')
def play():
    id = player_random()
    global juez
    global band
    if band==0:
        juez=juez_random()
        band=1
        Cblack_random()
    return redirect('jugar/{}'.format(id))
    #jugar(idj=id)


@app.route('/ver_Players')  # Parte de Ulises. Idealmente este link está dentro de un submenú o algo así
def fetch_players():
    cursor = mysql.connection.cursor()
    cursor.execute("SELECT * FROM jugador")
    data = cursor.fetchall()
    cursor.close()
    return render_template('player_index.html', players=data)


@app.route('/agg_player', methods=['POST'])  # Parte de Ulises
def agg_player():
    if request.method == 'POST':
        player_name = request.form['playerName']
        cursor = mysql.connection.cursor()
        cursor.execute("INSERT INTO jugador(nombreUsuario) VALUES (%s)", [player_name])
        mysql.connection.commit()
        flash('Se agregó correctamente el jugador')
        return redirect(url_for('fetch_players'))


@app.route('/edit_player/<player_id>', methods=['POST', 'GET'])  # Parte de Abelardo
def get_player(player_id):
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM jugador WHERE idJugador = {0}'.format(player_id))
    data = cur.fetchall()
    cur.close()
    return render_template('edit_player.html', player=data[0])


# Estas funciones que se encargan de redireccionar

@app.route('/index')
def inicio():
    return render_template('inicio.html')


@app.route('/beginGame')
def tablero():
    return render_template('juegos.html')


@app.route('/cartasPage')
def cartasABM():
    return render_template('index.html')


@app.route('/playerPage')
def editPlayers():
    return redirect(url_for('fetch_players'))


@app.route('/info')
def infoGame():
    return render_template('info.html')


@app.route('/update_player/<player_id>', methods=['POST'])  # Parte de Abelardo
def update_player(player_id):
    if request.method == 'POST':
        player_name = request.form['playerName']
        cur = mysql.connection.cursor()
        cur.execute("UPDATE jugador SET nombreUsuario = %s WHERE idJugador = %s", (player_name, player_id))
        flash('Actualizado correctamente')
        mysql.connection.commit()
        return redirect(url_for('fetch_players'))


@app.route('/delete_player/<player_id>', methods=['POST', 'GET'])  # Parte de Abelardo
def delete_player(player_id):
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM  jugador WHERE idJugador = {0}".format(player_id))
    flash('Eliminado correctamente')
    mysql.connection.commit()
    return redirect(url_for('fetch_players'))


@app.route('/guardarCarta', methods=['POST'])
def guardarCarta():
    if request.method == 'POST':
        name = request.form['type']
        name2 = request.form['content']
        name3 = request.form['content2']
        name4 = request.form['content3']
        cursor = mysql.connection.cursor()
        cursor.execute('insert into cartas (tipo) VALUES (%s)', (name))
        mysql.connection.commit()
        flash('Carta Ingresada')
        cursor.execute('SELECT idCartas FROM cartas ORDER BY idCartas DESC LIMIT 1')
        data = cursor.fetchall()
        if name2 != '':
            cursor.execute('insert into cartascontenidos (idCarta, contenido) VALUES (%s,%s)', (data, name2))
            mysql.connection.commit()
        if name3 != '':
            cursor.execute('insert into cartascontenidos (idCarta, contenido) VALUES (%s,%s)', (data, name3))
            mysql.connection.commit()
        if name4 != '':
            cursor.execute('insert into cartascontenidos (idCarta, contenido) VALUES (%s,%s)', (data, name4))
            mysql.connection.commit()
        cursor.close()
        return redirect(url_for('prueba'))


@app.route('/borrarCarta/<idCarta>', methods=['POST', 'GET'])  # borrar cartas
def borrarCarta(idCarta):
    cur = mysql.connection.cursor()
    cur.execute('DELETE FROM cartascontenidos WHERE idCarta = {0}'.format(idCarta))
    mysql.connection.commit()
    cur.execute('DELETE FROM cartas WHERE idCartas NOT IN (SELECT idCarta FROM cartascontenidos)'.format(idCarta))
    mysql.connection.commit()
    return redirect(url_for('prueba'))

@app.route('/jugar/<int:idj>')  # Fer, acá está el id del jugador, es global: player_array[0]
def jugar(idj):
    # cargarmano(idj, 5)
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM manojugador WHERE idJugador={}".format(idj))
    data = cur.fetchall()
    cur.close()
    global juez
    global contplayer
    global crespuestas
    print(juez)
    return render_template('juegos.html', cards=data, idj=int(idj), juez=int(juez), black_card=black_card, contplayer=contplayer-1, crespuestas=crespuestas)


@app.route('/juez', methods=['POST'])
def fjuez(id, idganador):
    if request.method == 'POST':
        cur = mysql.connection.cursor()
        cur.execute("UPDATE jugador SET puntuación=puntuación+1 WHERE idJugador='{}'".format(idganador))
        cur.execute("DELETE FROM cartaseleccionada")
        mysql.connection.commit()
        cur.close
        global juez
        juez = idganador
        print("nuevo juez " + str(juez))
        Cblack_random()
        return redirect('jugar/{}'.format(id))
    else:
        return 'uffs'


@app.route('/enviar', methods=['POST'])
def enviar():
    if request.method == 'POST':
        cards = request.form.getlist('card')
        id = request.form['idjugador']
        global juez
        print("enviar juez " + str(juez))
        print("id " + str(id))
        if int(id) == int(juez):
            idganador = request.form['cganador']
            return fjuez(id, idganador)
        if cards:
            print(cards)
            print(id)
            print(len(cards))
            cur = cur = mysql.connection.cursor()
            for card in cards:
                cur.execute('INSERT INTO cartaseleccionada (idcarta, idjugador) VALUES (%s, %s)', (card, id))
                cur.execute("UPDATE cartas SET estado=1 WHERE idCartas='{}'".format(id))
                cur.execute('DELETE FROM manojugador WHERE idCarta={}'.format(card))
            cargarmano(id, len(cards))
            mysql.connection.commit()
            cur.close()
            global crespuestas
            crespuestas=crespuestas+1

        else:
            flash('No seleccionó ninguna carta')
            return redirect('jugar/{}'.format(id))
        return redirect('jugar/{}'.format(id))
    return 'Ocurrio un error'


# funcion para asignar cartas al jugador, se recomienda usarla para cargar
# las primeras 5 cartas cuando el jugador seleccione su usurio, 
# luego el programa se encargara de renovar sus cartas
def cargarmano(idj, limit):
    cur = mysql.connection.cursor()
    # Estado (0=Acivo ; 1=ya fue utilizado)
    # falta el 'AND estado=0' para seleccionar solo las cartas aun no utilizadas, no lo puse por efectos practicos para las pruebas
    # LIMIT se usa para limitar a seleccionar solo una cantidad x de registros registros
    cur.execute('SELECT * FROM cartas WHERE tipo=0 LIMIT {}'.format(limit))
    data = cur.fetchall()
    for id in data:
        cur.execute("UPDATE cartas SET estado=1 WHERE idCartas='{}'".format(id[0]))
        cur.execute("INSERT INTO manojugador(idCarta, idJugador) VALUES (%s, %s)", (id[0], idj))
    mysql.connection.commit()
    cur.close()


@app.context_processor
def utility_processor():
    def mostrar(id):
        cur = mysql.connection.cursor()
        cur.execute("SELECT * FROM cartascontenidos WHERE idCarta='{}'".format(id))
        data = cur.fetchall()
        text = ''
        for part in data:
            if text == '':
                text = text + part[2]
            else:
                text = text + '_____________' + part[2]

        return text

    return dict(text=mostrar)


@app.context_processor
def cards():
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM cartaseleccionada')
    data = cur.fetchall()
    cur.close
    return dict(respuestas=data)


def juez_random():
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM jugador')
    data = cur.fetchall()
    data = random.choice(data)
    cur.close
    print('juez ' + str(data[0]))
    return data[0]


def Cblack_random():
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM cartas WHERE tipo=1 AND estado=0')
    data=cur.fetchall()
    if data:
        print(data)
        data = random.choice(data)
        cur.execute("UPDATE cartas SET estado=1 WHERE idCartas='{}'".format(data[0]))
        mysql.connection.commit()
        cur.close
        global black_card
        black_card = data[0]
        print("nueva carta negra " + str(black_card))
    return "NO HAY CARTAS LIBRES"


def player_random():  # Parte de Ulises
    global player_array
    cursor = mysql.connection.cursor()
    cursor.execute('SELECT * FROM jugador WHERE turno = 0 ')  # Hay que añadir el filtro WHERE turno == 0
    filas = cursor.rowcount
    if filas != 0:
        data = cursor.fetchall()
        player = random.choice(data)
        player_id = player[0]
        player_name = player[1]
        player_array = [player_id, player_name]
        cursor.execute("UPDATE jugador SET turno = %s WHERE idJugador = %s", (player_id, player_id))
        mysql.connection.commit()
        cargarmano(player_id, 5)
        global contplayer
        contplayer=contplayer+1
    else:
        print('Cantidad de jugadores máxima alcanzada!')
    return player_id

if __name__ == "__main__":
    app.run(port=3000, debug=True)
